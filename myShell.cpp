#include "myShell.h"

#define PWD "pwd"
#define CD "cd"
#define CREATE "create"
#define LS "ls"
#define SECRET "secret"

typedef int(__stdcall *f_funci)();

Helper help;

void myShell::mainShell()
{
	string str;
	vector<string> words;
	bool isCd = false;
	bool isCreate = false;
	while (true)
	{
		cout << ">> ";
		std::getline(std::cin, str);
		words = help.get_words(str);
		for each (string s in words)
		{
			if (s == PWD)
				cout << this->pwd() << endl;
			if (s == CD)
				isCd = true;
			if (s == CREATE)
				isCreate = true;
			if (isCd && s != CD)
				this->cd(s);
			if (isCreate && s != CREATE)
				this->create(s);
			if (s == LS)
				this->ls();
			if (s == SECRET)
				this->secret();
			if (s.find(".exe"))
				this->exec(s);
		}
		isCd = false;
		isCreate = false;
	}
}

string myShell::pwd()
{
	TCHAR NPath[MAX_PATH];
	GetCurrentDirectory(MAX_PATH, NPath);
	return string(NPath);
}

void myShell::cd(string path)
{

	std::replace(path.begin(), path.end(), '\\', '\\');
	const char* newDir = path.c_str();
	if (!SetCurrentDirectory(newDir))
		std::cerr << "Error setting current directory: #" << GetLastError() << endl;
}

void myShell::create(string fileName)
{
	HANDLE h = CreateFile(fileName.c_str(),GENERIC_WRITE, 0, 0, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL,0);
}

void myShell::ls()
{
	UINT counter(0);
	bool working(true);
	string buffer;
	WIN32_FIND_DATA data;
	HANDLE myHandle = FindFirstFile("*.*", &data);

	if (myHandle != INVALID_HANDLE_VALUE)
	{
		buffer = data.cFileName;

		while (working)
		{
			FindNextFile(myHandle, &data);
			if (data.cFileName != buffer)
			{
				buffer = data.cFileName;
				cout << buffer << endl;
			}
			else
			{
				working = false;
			}
		}
	}
}

void myShell::secret()
{
	HINSTANCE hGetProcIDDLL = LoadLibrary("Secret.dll");

	f_funci funci = (f_funci)GetProcAddress(hGetProcIDDLL, "TheAnswerToLifeTheUniverseAndEverything");
	if (!funci) {
		std::cout << "could not locate the function" << std::endl;
	}
	std::cout << "The answer to life is: " << funci() << std::endl;
}

void myShell::exec(string file)
{
	STARTUPINFO si;
	PROCESS_INFORMATION pi;
	LPDWORD l = 0;
	ZeroMemory(&si, sizeof(si));
	si.cb = sizeof(si);
	ZeroMemory(&pi, sizeof(pi));

	CreateProcess(file.c_str(),__argv[1],NULL,NULL,FALSE,0,NULL,NULL,&si,&pi);         

	WaitForSingleObject(pi.hProcess, INFINITE); //This function waits until an object acts.

	CloseHandle(pi.hProcess);
	CloseHandle(pi.hThread);
	GetExitCodeProcess(pi.hProcess, NULL); //This function retrieves the exit process argument.

}