#pragma once

#include <Windows.h>
#include <stdio.h>
#include <stdlib.h>
#include <tchar.h>
#include <iostream>
#include <string>
#include "Helper.h"

using namespace std;

class myShell
{
public:
	void mainShell();
	string pwd();
	void cd(string path);
	void create(string fileName);
	void ls();
	void secret();
	void exec(string file);
};